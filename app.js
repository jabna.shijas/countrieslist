document.addEventListener("DOMContentLoaded", () => {
    const countriesContainer = document.getElementById('countries-container');
    const searchBtn = document.querySelector(".btn");
    const inputField = document.getElementById("input");

    searchBtn.addEventListener("click", () => {
        const inputValue = inputField.value.trim().toLowerCase();
        if (inputValue.length === 0) {
            return; // Exit early if input is empty
        }

        fetch(`https://restcountries.com/v3.1/name/${inputValue}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Country not found');
                }
                return response.json();
            })
            .then(data => {
                displayCountries(data);
            })
            .catch(error => {
                console.error(error);
                countriesContainer.innerHTML = `<p>Error fetching country data. Please try again later.</p>`;
            });
    });

    const displayCountries = (countries) => {
        countriesContainer.innerHTML = ''; // Clear previous results

        countries.forEach(country => {
            const countryCard = document.createElement('article');
            countryCard.classList.add('country-card');
            countryCard.innerHTML = `
                <h4 class="country-name">${country.name.common}</h4>
                <img src="${country.flags.png}" >
            `;
            countriesContainer.appendChild(countryCard);
        });
    };

    const getData = () => {
        fetch('https://restcountries.com/v3.1/all')
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to fetch countries');
                }
                return response.json();
            })
            .then(data => {
                displayCountries(data);
            })
            .catch(error => {
                console.error('Error fetching countries data:', error);
                countriesContainer.innerHTML = '<p>Error fetching country data. Please try again later.</p>';
            });
    };

    // Call getData function when the DOM content is loaded
    getData();
});
